import React, { useEffect, useState } from 'react';

function ShoeList(props) {

    const [shoes, setShoes] = useState([])

       
        const fetchData = async () => {
        const url = `http://localhost:8080/api/shoes`;
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes);
        }
    }


    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8080/api/shoes/${id}/`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
            setShoes(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Manufacturer</th>
                <th>Color</th>
                <th>Model Name</th>
                <th>Picture</th>
                <th>Bin</th>
            </tr>
            </thead>
            <tbody>
            {shoes.map(i => {
                return (
                <tr key={i.id}>
                    <td>{ i.manufacturer }</td>
                    <td>{ i.color }</td>
                    <td>{ i.model_name }</td>
                    <td><img src ={i.picture_url} alt="" height="100"></img></td>
                    <td>{ i.bin.closet_name }</td>
                    <td><button onClick={() => handleDelete(i.id)}>Delete</button></td>
                </tr>
                );
            })}
            </tbody>
        </table>
        );
    }

    export default ShoeList;
