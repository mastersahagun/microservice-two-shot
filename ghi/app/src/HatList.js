import React, { useEffect, useState } from 'react';

function HatList(props) {
    const [hats, setHats] = useState([])
    const fetchData = async () => {
        const url = `http://localhost:8090/api/hats`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setHats(data.hats);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleDelete = (id) => {
        fetch(`http://localhost:8090/api/locations/${id}/hat`, { method: 'DELETE' })
        .then(response => {
            if (response.ok) {
                setHats(prevResources => prevResources.filter(resource => resource.id !== id));
            }
        })
        .catch(error => console.error('Error deleting resource:', error));
    };

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>ClosetName</th>
                    <th>Hat Name</th>
                    <th>Color</th>
                    <th>Fabric</th>
                    <th>Picture</th>
                </tr>
            </thead>
            <tbody>
                {hats.map(i => {
                    return (
                    <tr key={i.id}>
                        <td>{ i.location.closet_name }</td>
                        <td>{ i.name }</td>
                        <td>{ i.color }</td>
                        <td>{ i.fabric }</td>
                        <td><img src ={i.picture_url} alt="Hat" height="100"/></td>
                        <td><button onClick={() => handleDelete(i.id)}>Delete</button></td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        );
    }

    export default HatList;
