import React, { useEffect, useState } from 'react';


function ShoeForm() {
    const [bins, setBins] = useState([])
    const [formData, setFormData] = useState({
        manufacturer : '',
        model_name : '',
        color : '',
        picture_url : '',
        bin : '',
    })


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setBins(data.bins);
        }
    }


    useEffect(() => {
    fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/shoes/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json'
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {

            setFormData({
                manufacturer : '',
                model_name : '',
                color : '',
                picture_url : '',
                bin : '',
            });
            event.target.reset();
        }
    }


    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>

                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Manufacturer" required type="text" id="manufacturer" name="manufacturer" className="form-control"/>
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Model Name" required type="text" id="model_name" name="model_name" className="form-control"/>
                        <label htmlFor="model_name">Model Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Shoe Color" type="text" id="color" name="color" className="form-control"/>
                        <label htmlFor="color">Shoe Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Shoe Image URL" required type="url" id="picture_url" name="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Shoe Image URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="bin" name="bin" className="form-select">

                            <option>Choose a Bin</option>
                            
                            {bins.map(bin => {
                    return (
                        <option key={bin.href} value={bin.href}>{bin.closet_name}</option>
                    )
                    })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default ShoeForm;
