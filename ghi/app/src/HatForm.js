import React, { useEffect, useState } from 'react';

function HatForm() {
    const [locations, setLocations] = useState([])
    const [formData, setFormData] = useState({
        name: '',
        color: '',
        fabric: '',
        picture_url: '',
        location: '',
    })

    const fetchData = async () => {
        const url = `http://localhost:8100/api/locations`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = `http://localhost:8090/api/hats`;
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(url, fetchConfig);

        if (response.ok) {
            setFormData({
                name: '',
                color: '',
                fabric: '',
                picture_url: '',
                location: '',
            });
            event.target.reset();
        }
    }

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit={handleSubmit} id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Name" required type="text" id="name" name="name" className="form-control"/>
                        <label htmlFor="name">Hat Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Color" required type="text" id="color" name="color" className="form-control"/>
                        <label htmlFor="color">Hat Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Hat Fabric" type="text" id="fabric" name="fabric" className="form-control"/>
                        <label htmlFor="fabric">Hat Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFormChange} placeholder="Hat URL" required type="url" id="picture_url" name="picture_url" className="form-control"/>
                        <label htmlFor="picture_url">Hat Image URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleFormChange} required id="location" name="location" className="form-select">
                            <option>Choose a Location</option>
                            {locations.map(i => {
                    return (
                        <option key={i.href} value={i.href}>{i.closet_name}</option>
                    )
                    })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
            </div>
        </div>
    );
}

export default HatForm;
