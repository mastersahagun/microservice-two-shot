from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import HatsInfo, LocationVO


class LocationDetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "section_number",
        "shelf_number",
        "import_href",
    ]


class HatEncoder(ModelEncoder):
    model = HatsInfo
    properties = [
        "name",
        "id",
        "color",
        "fabric",
        "picture_url",
        "location",
    ]
    encoders = {
        "location": LocationDetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_hats(request):

    if request.method == "GET":
        hats = HatsInfo.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid location id"},
                status=400,
            )
        hat = HatsInfo.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_hat(request, pk):
    if request.method == "GET":
        try:
            hat = HatsInfo.objects.get(id=pk)
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False
            )
        except HatsInfo.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            hat = HatsInfo.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except HatsInfo.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            hat = HatsInfo.objects.get(id=pk)

            try:
                location = LocationVO.objects.get(import_href=content["location"])
                content["location"] = location
            except LocationVO.DoesNotExist:
                return JsonResponse(
                    {"message": "Invalid location id"},
                    status=400,
                )

            props = [
                "fabric",
                "name",
                "color",
                "picture_url",
                "id",
                "location",
                ]
            for prop in props:
                if prop in content:
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except HatsInfo.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
