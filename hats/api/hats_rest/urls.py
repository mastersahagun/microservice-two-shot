from django.urls import path
from .views import api_hats, api_hat

urlpatterns = [
    path("hats", api_hats, name="api_hats"),
    path("locations/<int:pk>/hat", api_hat, name="api_hat"),
]
