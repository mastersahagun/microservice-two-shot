import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()


from hats_rest.models import LocationVO


def get_hats():
    url = "http://wardrobe-api:8000/api/locations/"
    response = requests.get(url)
    content = json.loads(response.content)

    for i in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=i["href"],
            defaults={
                "closet_name": i["closet_name"],
                "section_number": i["section_number"],
                "shelf_number": i["shelf_number"]
                },
        )


def poll():
    while True:
        print('Hats poller polling for data')
        try:
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
