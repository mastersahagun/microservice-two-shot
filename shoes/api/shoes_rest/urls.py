from django.urls import path
from .views import SingleShoe, ListShoes


urlpatterns = [
    path("shoes/", ListShoes, name="ListShoes"),
    path("shoes/<int:id>/", SingleShoe, name="SingleShoe")
]
