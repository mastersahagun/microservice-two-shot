from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from .models import ShoeInfo, BinVO
from common.json  import ModelEncoder
from django.http import JsonResponse



class BinEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size",
        "import_href",
    ]


class ShoeEncoder(ModelEncoder):
    model = ShoeInfo
    properties = [
        "id",
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinEncoder(),
    }


@require_http_methods(["GET", "POST"])
def ListShoes(request):
    if request.method == "GET":
        shoes = ShoeInfo.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeEncoder,
        )
    else:
        content = json.loads(request.body)

        try:
            bin = BinVO.objects.get(import_href=content["bin"])
            content["bin"] = bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = ShoeInfo.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def SingleShoe(request, id):
    if request.method == "GET":
        shoe = ShoeInfo.objects.get(id=id)
        return JsonResponse(
            shoe,
            encoder=ShoeEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        count, _ = ShoeInfo.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count > 0})
